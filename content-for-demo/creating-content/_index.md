---
title: "Creating Content"
date: 2021-03-07T08:22:44+11:00
draft: false
weight: 20
---

Create content in Markdown. Note [other formats](https://gohugo.io/content-management/formats/), including reStructuredText and AsciiDoc, are also supported.

Make sure to include front matter at the top of each file

This example uses the Geekdoc theme and order to get the Table of contents to display correctly we need to use the Hugo [page bundles](https://gohugo.io/content-management/page-bundles/).

Here is the workflow to get markdowm into a public website

{{< mermaid class="text-center">}}
graph TD
    writer --> |commit changes with Git| git[(Version control repo)]
    git --> |Checkout| files[/Working copy/]
    files --> |Hugo generates| content[/Static Web content/]
    content --> |Web Server| Reader
{{< /mermaid >}}

This digram was generated with some simple text

```
{{</* mermaid class="text-center" */>}}
graph TD
    writer --> |commit changes with Git| git[(Version control repo)]
    git --> |Checkout| files[/Working copy/]
    files --> |Hugo generates| content[/Static Web content/]
    content --> |Web Server| Readerr
{{</* /mermaid */>}}
```