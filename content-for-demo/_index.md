---
title: "Hugo and Open API Specifications"
date: 2021-03-06T13:15:03+11:00
draft: false
---

![Hugo Logo](https://d33wubrfki0l68.cloudfront.net/c38c7334cc3f23585738e40334284fddcaf03d5e/2e17c/images/hugo-logo-wide.svg)

The website provides a simple example of using the Hugo static site generator
to serve API documentation.

